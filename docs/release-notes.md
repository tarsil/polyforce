# Release Notes

## 0.2.0

### Changed

* Refactor the whole core of decorator.
* Refactor the whole core of PolyModel.
* Refactor base metaclasse of PolyModel.
* Update tests.
* Add new `ValidationError` returning human readable messages.

## 0.1.0

Initial release of the **Polyforce**.
